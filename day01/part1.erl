-module(part1).
-export([main/0]).
-export([test/0]).


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [binary_to_integer(Line) || Line <- Lines].


find_entries(Target, [Head|Tail]) ->
    case ordsets:is_element(Target - Head, Tail) of
        true -> {Head, Target - Head};
        false -> find_entries(Target, Tail)
    end.


find_entries_product(Numbers) ->
    Set = ordsets:from_list(Numbers),
    {A, B} = find_entries(2020, Set),
    A * B.


test() ->
    Numbers = read_input("test.txt"),
    Value = find_entries_product(Numbers),
    io:fwrite("~w~n", [Value]).


main() ->
    Numbers = read_input("input.txt"),
    Value = find_entries_product(Numbers),
    io:fwrite("~w~n", [Value]).
