-module(part1).
-export([main/0]).
-export([test/0]).


parse_line(Line) ->
    % 6-10 s: snkscgszxsssscss
    [Min, Tail1] = binary:split(Line, <<"-">>),
    [Max, Tail2] = binary:split(Tail1, <<" ">>),
    [Char, Tail3] = binary:split(Tail2, <<":">>),
    [_, Password] = binary:split(Tail3, <<" ">>),
    {binary_to_integer(Min), binary_to_integer(Max), Char, Password}.


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [parse_line(Line) || Line <- Lines].


count(Needle, Haystack) -> count(Needle, Haystack, 0).
count(_, <<>>, Count) -> Count;
count(<<X>>, <<X,Rest/binary>>, Count) -> count(<<X>>, Rest, Count+1);
count(<<X>>, <<_,Rest/binary>>, Count) -> count(<<X>>, Rest, Count).


is_valid({Min, Max, Char, Password}) ->
    Count = count(Char, Password),
    (Count >= Min) and (Count =< Max).


total_valid_passwords(Lines) ->
    lists:foldl(fun(X, Sum) ->
        case is_valid(X) of
            true -> Sum + 1;
            false -> Sum
        end
    end, 0, Lines).


test() ->
    Numbers = read_input("test.txt"),
    Value = total_valid_passwords(Numbers),
    io:fwrite("~w~n", [Value]).


main() ->
    Numbers = read_input("input.txt"),
    Value = total_valid_passwords(Numbers),
    io:fwrite("~w~n", [Value]).
