-module(part2).
-export([main/0]).
-export([test/0]).


parse_line(Line) ->
    % 6-10 s: snkscgszxsssscss
    [Pos1, Tail1] = binary:split(Line, <<"-">>),
    [Pos2, Tail2] = binary:split(Tail1, <<" ">>),
    [Char, Tail3] = binary:split(Tail2, <<":">>),
    [_, Password] = binary:split(Tail3, <<" ">>),
    {binary_to_integer(Pos1) - 1, binary_to_integer(Pos2) - 1, Char, Password}.


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [parse_line(Line) || Line <- Lines].


is_valid({Pos1, Pos2, <<Char>>, Password}) ->
    case {binary:at(Password, Pos1), binary:at(Password, Pos2)} of
        {Char, Char} -> false;
        {Char, _} -> true;
        {_, Char} -> true;
        {_, _} -> false
    end.


total_valid_passwords(Lines) ->
    lists:foldl(fun(X, Sum) ->
        case is_valid(X) of
            true -> Sum + 1;
            false -> Sum
        end
    end, 0, Lines).


test() ->
    Numbers = read_input("test.txt"),
    Value = total_valid_passwords(Numbers),
    io:fwrite("~w~n", [Value]).


main() ->
    Numbers = read_input("input.txt"),
    Value = total_valid_passwords(Numbers),
    io:fwrite("~w~n", [Value]).
