-module(part1).
-export([main/0]).
-export([test/0]).


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    binary:split(Data, <<"\n">>, [global, trim]).


is_hit(Line, {Index, Hits}) ->
    case binary:at(Line, (3 * Index) rem byte_size(Line)) of
        $# -> {Index + 1, Hits +1};
        $. -> {Index + 1, Hits}
    end.


count_hits(Lines) ->
    {_, Hits} = lists:foldl(fun is_hit/2, {0, 0}, Lines),
    Hits.

test() ->
    Lines = read_input("test.txt"),
    Value = count_hits(Lines),
    io:fwrite("~w~n", [Value]).


main() ->
    Lines = read_input("input.txt"),
    Value = count_hits(Lines),
    io:fwrite("~w~n", [Value]).
