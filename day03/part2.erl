-module(part2).
-export([main/0]).
-export([test/0]).


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    binary:split(Data, <<"\n">>, [global, trim]).


is_hit(Line, {Index, Hits}, {Right, Down}) when Index rem Down =:= 0 ->
    case binary:at(Line, (Right * (Index div Down)) rem byte_size(Line)) of
        $# -> {Index + 1, Hits +1};
        $. -> {Index + 1, Hits}
    end;

is_hit(_, {Index, Hits}, _) ->
    {Index + 1, Hits}.


make_is_hit(Slope) ->
    fun (Line, Acc) ->
        is_hit(Line, Acc, Slope)
    end.


count_hits(Lines, Slope) ->
    {_, Hits} = lists:foldl(make_is_hit(Slope), {0, 0}, Lines),
    Hits.


product_of_hits(Lines) ->
    Slopes = [{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}],
    lists:foldl(fun (Slope, Acc) -> Acc * count_hits(Lines, Slope) end, 1, Slopes).


test() ->
    Lines = read_input("test.txt"),
    Value = product_of_hits(Lines),
    io:fwrite("~w~n", [Value]).


main() ->
    Lines = read_input("input.txt"),
    Value = product_of_hits(Lines),
    io:fwrite("~w~n", [Value]).
