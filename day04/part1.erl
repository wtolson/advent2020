-module(part1).
-export([main/0]).
-export([test/0]).


parse_passport_fields(Passport) ->
    Fields = binary:split(Passport, [<<"\n">>, <<" ">>], [global, trim_all]),
    sets:from_list([hd(binary:split(Item, <<":">>)) || Item <- Fields]).


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Passports = binary:split(Data, <<"\n\n">>, [global, trim]),
    [parse_passport_fields(Passport) || Passport <- Passports].


is_valid(PassportFields) ->
    RequiredFields = sets:from_list([
        <<"byr">>, <<"iyr">>, <<"eyr">>, <<"hgt">>, <<"hcl">>, <<"ecl">>, <<"pid">>]),
    sets:is_subset(RequiredFields, PassportFields).


total_valid_passports(Passports) ->
    lists:foldl(fun(Passport, Sum) ->
        case is_valid(Passport) of
            true -> Sum + 1;
            false -> Sum
        end
    end, 0, Passports).


test() ->
    Passports = read_input("test.txt"),
    Value = total_valid_passports(Passports),
    io:fwrite("~w~n", [Value]).


main() ->
    Passports = read_input("input.txt"),
    Value = total_valid_passports(Passports),
    io:fwrite("~w~n", [Value]).
