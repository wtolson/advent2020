-module(part2).
-export([main/0]).
-export([test/0]).


parse_field(Field) ->
    [Name, Value] = binary:split(Field, <<":">>),
    {Name, Value}.


parse_passport_fields(Passport) ->
    Fields = binary:split(Passport, [<<"\n">>, <<" ">>], [global, trim_all]),
    [parse_field(Field) || Field <- Fields].


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Passports = binary:split(Data, <<"\n\n">>, [global, trim]),
    [parse_passport_fields(Passport) || Passport <- Passports].


% byr (Birth Year) - four digits; at least 1920 and at most 2002.
valid_byr(undefined) -> false;
valid_byr(Value) ->
    Year = binary_to_integer(Value),
    (byte_size(Value) =:= 4) andalso (Year >= 1920) andalso (Year =< 2002).


% iyr (Issue Year) - four digits; at least 2010 and at most 2020.
valid_iyr(undefined) -> false;
valid_iyr(Value) ->
    Year = binary_to_integer(Value),
    (byte_size(Value) =:= 4) andalso (Year >= 2010) andalso (Year =< 2020).


% eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
valid_eyr(undefined) -> false;
valid_eyr(Value) ->
    Year = binary_to_integer(Value),
    (byte_size(Value) =:= 4) andalso (Year >= 2020) andalso (Year =< 2030).


% If cm, the number must be at least 150 and at most 193.
valid_cm(Value) ->
    (Value >= 150) andalso (Value =< 193).


% If in, the number must be at least 59 and at most 76.
valid_in(Value) ->
    (Value >= 59) andalso (Value =< 76).


% hgt (Height) - a number followed by either cm or in:
valid_hgt(undefined) -> false;
valid_hgt(Value) ->
    case string:to_integer(Value) of
        {Hight, <<"cm">>} -> valid_cm(Hight);
        {Hight, <<"in">>} -> valid_in(Hight);
        _ -> false
    end.


% hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
valid_hcl(undefined) -> false;
valid_hcl(Value) ->
    case re:run(Value, "^#[a-f0-9]{6}$") of
        {match, _} -> true;
        nomatch -> false
    end.


% ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
valid_ecl(undefined) -> false;
valid_ecl(Value) ->
    Choices = sets:from_list(
        [<<"amb">>, <<"blu">>, <<"brn">>, <<"gry">>, <<"grn">>, <<"hzl">>, <<"oth">>]),
    sets:is_element(Value, Choices).


% pid (Passport ID) - a nine-digit number, including leading zeroes.
valid_pid(undefined) -> false;
valid_pid(Value) ->
    case re:run(Value, "^[0-9]{9}$") of
        {match, _} -> true;
        nomatch -> false
    end.


is_valid(Passport) ->
    % byr (Birth Year) - four digits; at least 1920 and at most 2002.
    % iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    % eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    % hgt (Height) - a number followed by either cm or in:
    % If cm, the number must be at least 150 and at most 193.
    % If in, the number must be at least 59 and at most 76.
    % hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    % ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    % pid (Passport ID) - a nine-digit number, including leading zeroes.
    % cid (Country ID) - ignored, missing or not.
    valid_byr(proplists:get_value(<<"byr">>, Passport)) andalso
    valid_iyr(proplists:get_value(<<"iyr">>, Passport)) andalso
    valid_eyr(proplists:get_value(<<"eyr">>, Passport)) andalso
    valid_hgt(proplists:get_value(<<"hgt">>, Passport)) andalso
    valid_hcl(proplists:get_value(<<"hcl">>, Passport)) andalso
    valid_ecl(proplists:get_value(<<"ecl">>, Passport)) andalso
    valid_pid(proplists:get_value(<<"pid">>, Passport)).


total_valid_passports(Passports) ->
    lists:foldl(fun(Passport, Sum) ->
        case is_valid(Passport) of
            true -> Sum + 1;
            false -> Sum
        end
    end, 0, Passports).


test() ->
    InvalidPassports = read_input("invalid.txt"),
    TotalInvalid = total_valid_passports(InvalidPassports),
    ValidPassports = read_input("valid.txt"),
    TotalValid = total_valid_passports(ValidPassports),
    io:fwrite("~w ~w~n", [TotalInvalid, TotalValid]).


main() ->
    Passports = read_input("input.txt"),
    Value = total_valid_passports(Passports),
    io:fwrite("~w~n", [Value]).
