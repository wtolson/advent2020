-module(day05).
-export([test/0]).
-export([part1/0]).
-export([part2/0]).


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    binary:split(Data, <<"\n">>, [global, trim]).


to_bit($F) -> 0;
to_bit($B) -> 1;
to_bit($L) -> 0;
to_bit($R) -> 1.


find_seat(Pass) ->
    <<Row:7, Col:3>> = << <<(to_bit(X)):1>> || <<X>> <= Pass >>,
    {Row, Col}.


find_seat_id(Row, Col) ->
    (8 * Row) + Col.


find_seat_id(Pass) ->
    {Row, Col} = find_seat(Pass),
    find_seat_id(Row, Col).


find_seat_and_id(Pass) ->
    {Row, Col} = find_seat(Pass),
    {Row, Col, find_seat_id(Row, Col)}.


find_max_seat_id(Passes) ->
    lists:max([find_seat_id(Pass) || Pass <- Passes]).


search_seats([A, B | _]) when B - A == 2 ->
    A + 1;

search_seats([_ | Tail]) ->
    search_seats(Tail).


find_missing_seat_id(Passes) ->
    Ids = lists:sort([find_seat_id(Pass) || Pass <- Passes]),
    search_seats(Ids).


test() ->
    {44, 5, 357} = find_seat_and_id(<<"FBFBBFFRLR">>),
    {70, 7, 567} = find_seat_and_id(<<"BFFFBBFRRR">>),
    {14, 7, 119} = find_seat_and_id(<<"FFFBBBFRRR">>),
    {102, 4, 820} = find_seat_and_id(<<"BBFFBBFRLL">>),
    ok.


part1() ->
    Passes = read_input("input.txt"),
    Value = find_max_seat_id(Passes),
    io:fwrite("~w~n", [Value]).


part2() ->
    Passes = read_input("input.txt"),
    Value = find_missing_seat_id(Passes),
    io:fwrite("~w~n", [Value]).
