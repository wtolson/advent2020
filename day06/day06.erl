-module(day06).
-export([test/0, part1/0, part2/0]).


parse_group(Group) ->
    People = binary:split(Group, <<"\n">>, [global, trim]),
    [sets:from_list(binary_to_list(Answers)) || Answers <- People].

read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Groups = binary:split(Data, <<"\n\n">>, [global, trim]),
    [parse_group(Group) || Group <- Groups].


total_unique(Group) ->
    sets:size(sets:union(Group)).


total_consistent(Group) ->
    sets:size(sets:intersection(Group)).


test() ->
    3 = total_unique(parse_group(<<"abc">>)),
    3 = total_unique(parse_group(<<"a\nb\nc">>)),
    3 = total_unique(parse_group(<<"ab\nac">>)),
    1 = total_unique(parse_group(<<"a\na\na\na">>)),
    1 = total_unique(parse_group(<<"b">>)),


    3 = total_consistent(parse_group(<<"abc">>)),
    0 = total_consistent(parse_group(<<"a\nb\nc">>)),
    1 = total_consistent(parse_group(<<"ab\nac">>)),
    1 = total_consistent(parse_group(<<"a\na\na\na">>)),
    1 = total_consistent(parse_group(<<"b">>)),

    ok.


part1() ->
    Groups = read_input("input.txt"),
    Value = lists:sum([total_unique(Group) || Group <- Groups]),
    io:fwrite("~w~n", [Value]).


part2() ->
    Groups = read_input("input.txt"),
    Value = lists:sum([total_consistent(Group) || Group <- Groups]),
    io:fwrite("~w~n", [Value]).
