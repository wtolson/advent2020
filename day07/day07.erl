-module(day07).
-export([test/0, part1/0, part2/0]).


parse_bag(Data) ->
    {match, [Number, Description]} = re:run(
        Data, "([0-9]+) (.+) bags?", [{capture, all_but_first, binary}]),
    {binary_to_integer(Number), Description}.


parse_bags(<<"no other bags.">>) -> [];
parse_bags(Data) ->
    [parse_bag(Bag) || Bag <- binary:split(Data, <<", ">>, [global])].


parse_rule(Line) ->
    [Container, Bags] = binary:split(Line, <<" bags contain ">>),
    {Container, parse_bags(Bags)}.


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [parse_rule(Line) || Line <- Lines].


merge_rule({Container, Bags}, Map) ->
    lists:foldl(
        fun ({_, Bag}, Acc) ->
            Values = maps:get(Bag, Acc, []),
            maps:put(Bag, ordsets:add_element(Container, Values), Acc)
        end, Map, Bags).


invert_rules(Rules) ->
    lists:foldl(fun merge_rule/2, maps:new(), Rules).


find_containers([], _, _, Acc) -> Acc;
find_containers([Target | Tail], Map, Seen, Acc) ->
    Values = maps:get(Target, Map, []),
    find_containers(
        ordsets:union(ordsets:subtract(Values, Seen), Tail), Map,
        ordsets:add_element(Target, Seen), ordsets:union(Acc, Values)).


find_containers(Target, Rules) ->
    find_containers([Target], invert_rules(Rules), [], []).


total_containers(Target, Rules) ->
    ordsets:size(find_containers(Target, Rules)).


bag_count(Target, Rules) ->
    lists:foldl(
        fun ({Number, Bag}, Acc) ->
            Acc + (Number * bag_count(Bag, Rules))
        end, 1, maps:get(Target, Rules, [])).


total_contents(Target, Rules) ->
    RuleMap = maps:from_list(Rules),
    bag_count(Target, RuleMap) - 1.


test() ->
    Rules = read_input("test.txt"),
    4 = total_containers(<<"shiny gold">>, Rules),
    32 = total_contents(<<"shiny gold">>, Rules),
    ok.


part1() ->
    Rules = read_input("input.txt"),
    Value = total_containers(<<"shiny gold">>, Rules),
    io:fwrite("~w~n", [Value]).


part2() ->
    Rules = read_input("input.txt"),
    Value = total_contents(<<"shiny gold">>, Rules),
    io:fwrite("~w~n", [Value]).
