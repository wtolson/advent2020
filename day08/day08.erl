-module(day08).
-export([test/0, part1/0, part2/0]).


parse_op(<<"nop">>) -> nop;
parse_op(<<"acc">>) -> acc;
parse_op(<<"jmp">>) -> jmp.


parse_instruction(Line) ->
    [Op, Arg] = binary:split(Line, <<" ">>),
    {parse_op(Op), binary_to_integer(Arg)}.


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    array:from_list([parse_instruction(Line) || Line <- Lines]).


run(Program, Pos, Acc) ->
    case array:get(Pos, Program) of
        {nop, _} -> {Pos + 1, Acc};
        {acc, Arg} -> {Pos + 1, Acc + Arg};
        {jmp, Arg} -> {Pos + Arg, Acc}
    end.


find_loop(Program, Pos, Acc, History) ->
    {NextPos, NextAcc} = run(Program, Pos, Acc),
    case sets:is_element(NextPos, History) of
        true -> Acc;
        false -> find_loop(Program, NextPos, NextAcc, sets:add_element(Pos, History))
    end.


find_loop(Program) ->
    find_loop(Program, 0, 0, sets:new()).


finish_run(Program, Pos, Acc, History) ->
    case array:size(Program) of
        Pos -> {ok, Acc};
        Size when Pos > Size -> error;
        _ ->
            {NextPos, NextAcc} = run(Program, Pos, Acc),
            case sets:is_element(NextPos, History) of
                true -> error;
                false ->
                    finish_run(Program, NextPos, NextAcc, sets:add_element(Pos, History))
            end
    end.


try_fix(Program, Pos, Op, Arg, Acc, History) ->
    NewProgram = array:set(Pos, {Op, Arg}, Program),
    case finish_run(NewProgram, Pos, Acc, History) of
        {ok, Result} -> Result;
        error ->
            {NextPos, NextAcc} = run(Program, Pos, Acc),
            NextHistory = sets:add_element(Pos, History),
            fix_and_run(Program, NextPos, NextAcc, NextHistory)
    end.



fix_and_run(Program, Pos, Acc, History) ->
    NextHistory = sets:add_element(Pos, History),
    case array:get(Pos, Program) of
        {acc, Arg} -> fix_and_run(Program, Pos + 1, Acc + Arg, NextHistory);
        {nop, Arg} -> try_fix(Program, Pos, jmp, Arg, Acc, NextHistory);
        {jmp, Arg} -> try_fix(Program, Pos, nop, Arg, Acc, NextHistory)
    end.


fix_and_run(Program) ->
    fix_and_run(Program, 0, 0, sets:new()).


test() ->
    Program = read_input("test.txt"),
    5 = find_loop(Program),
    8 = fix_and_run(Program),
    ok.


part1() ->
    Program = read_input("input.txt"),
    Value = find_loop(Program),
    io:fwrite("~w~n", [Value]).


part2() ->
    Program = read_input("input.txt"),
    Value = fix_and_run(Program),
    io:fwrite("~w~n", [Value]).
