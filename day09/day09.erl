-module(day09).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [binary_to_integer(Line) || Line <- Lines].


contains_sum(_, []) -> false;
contains_sum(Value, [Head | Tail]) ->
    case ordsets:is_element(Value - Head, Tail) of
        true -> true;
        false -> contains_sum(Value, Tail)
    end.


is_valid(Value, Preamble) ->
    contains_sum(Value, ordsets:from_list(Preamble)).


find_first_invalid(PreambleLength, Sequence) ->
    {Preamble, [Value | _]} = lists:split(PreambleLength, Sequence),
    case is_valid(Value, Preamble) of
        false -> Value;
        true -> find_first_invalid(PreambleLength, tl(Sequence))
    end.


find_contiguous_set(_, [], _) -> error;
find_contiguous_set(Target, [Head | _], _) when Target < Head -> error;
find_contiguous_set(Target, [Target | _], Acc) -> [Target | Acc];
find_contiguous_set(Target, [Head | Tail], Acc) ->
    find_contiguous_set(Target - Head, Tail, [Head | Acc]).

find_contiguous_set(Target, Sequence) ->
    case find_contiguous_set(Target, Sequence, []) of
        error -> find_contiguous_set(Target, tl(Sequence));
        [_] -> find_contiguous_set(Target, tl(Sequence));
        Result -> Result
    end.


find_weakness(PreambleLength, Sequence) ->
    Target = find_first_invalid(PreambleLength, Sequence),
    Set = find_contiguous_set(Target, Sequence),
    lists:min(Set) + lists:max(Set).


test() ->
    Sequence = read_input("test.txt"),
    ?assertEqual(127, find_first_invalid(5, Sequence)),
    ?assertEqual(62, find_weakness(5, Sequence)).


part1() ->
    Sequence = read_input("input.txt"),
    Value = find_first_invalid(25, Sequence),
    io:fwrite("~w~n", [Value]).


part2() ->
    Sequence = read_input("input.txt"),
    Value = find_weakness(25, Sequence),
    io:fwrite("~w~n", [Value]).
