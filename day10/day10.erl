-module(day10).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [binary_to_integer(Line) || Line <- Lines].


count_diffs([_], Ones, Threes) -> {Ones, Threes};
count_diffs([H1, H2 | Tail], Ones, Threes) ->
    case H2 - H1 of
        1 -> count_diffs([H2 | Tail], Ones + 1, Threes);
        3 -> count_diffs([H2 | Tail], Ones, Threes + 1)
    end.


find_chain_diffs(Adapters) ->
    count_diffs([0 | lists:sort(Adapters)], 0, 1).


get_combinatons(Adaptor, Combinations) ->
    maps:get(Adaptor - 1, Combinations, 0) +
    maps:get(Adaptor - 2, Combinations, 0) +
    maps:get(Adaptor - 3, Combinations, 0).


count_arrangements([Adapter], Combinations) ->
    get_combinatons(Adapter, Combinations);

count_arrangements([Adapter | Tail], Combinations) ->
    count_arrangements(Tail, Combinations#{Adapter => get_combinatons(Adapter, Combinations)}).

count_arrangements(Adapters) ->
    count_arrangements(lists:sort(Adapters), #{0 => 1}).


test() ->
    Adapters = read_input("test.txt"),
    ?assertEqual({22, 10}, find_chain_diffs(Adapters)),
    ?assertEqual(19208, count_arrangements(Adapters)).


part1() ->
    Adapters = read_input("input.txt"),
    {Ones, Threes} = find_chain_diffs(Adapters),
    io:fwrite("~w~n", [Ones * Threes]).


part2() ->
    Adapters = read_input("input.txt"),
    Value = count_arrangements(Adapters),
    io:fwrite("~w~n", [Value]).
