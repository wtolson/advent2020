-module(day11).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


parse_square($.) -> floor;
parse_square($L) -> empty;
parse_square($#) -> occupied.


parse_row(Line) ->
    [parse_square(Square) || <<Square>> <= Line].


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [parse_row(Line) || Line <- Lines].


read_grid(Filename) ->
    array:from_list(lists:append(read_input(Filename))).


lookup(X, Y, Height, Width, _Data) when X < 0; Y < 0; X >= Width; Y >= Height -> outofbounds;
lookup(X, Y, _Height, Width, Data) -> array:get(X + (Width * Y), Data).


neighbors(X, Y, Height, Width, Data) ->
    Candidates = [{X + DX, Y + DY} || DX <- [-1, 0, 1], DY <- [-1, 0, 1], (DX bor DY) =/= 0],
    % Neighbors must have a valid coordinate on the grid
    Valid = [NX + (Width * NY) || {NX, NY} <- Candidates, NX >= 0, NY >= 0, NX < Width, NY < Height],
    % Go ahead and filter out any neighbors that are floor as they'll never add to the count
    [Index || Index <- Valid, array:get(Index, Data) =/= foor].


find_lineofsight_seat(DX, DY, X, Y, Height, Width, Data) ->
    {NX, NY} = {X + DX, Y + DY},
    % io:fwrite("(~w ~w) ~w~n", [NX, NY, lookup(NX, NY, Height, Width, Data)]),
    case lookup(NX, NY, Height, Width, Data) of
        outofbounds -> none;
        floor -> find_lineofsight_seat(DX, DY, NX, NY, Height, Width, Data);
        _ -> NX + (Width * NY)
    end.


lineofsight_neighbors(X, Y, Height, Width, Data) ->
    Directions = [{DX, DY} || DX <- [-1, 0, 1], DY <- [-1, 0, 1], (DX bor DY) =/= 0],
    Neighbors = [find_lineofsight_seat(DX, DY, X, Y, Height, Width, Data) || {DX, DY} <- Directions],
    [Index || Index <- Neighbors, Index =/= none].


build_grid_neighbors(Height, Width, Data) ->
    array:from_list([neighbors(X, Y, Height, Width, Data) || Y <- lists:seq(0, Height - 1), X <- lists:seq(0, Width - 1)]).


build_lineofsight_neighbors(Height, Width, Data) ->
    array:from_list([lineofsight_neighbors(X, Y, Height, Width, Data) || Y <- lists:seq(0, Height - 1), X <- lists:seq(0, Width - 1)]).


build_grid(Rows) ->
    Height = length(Rows),
    Width = length(hd(Rows)),
    Grid = array:from_list(lists:append(Rows)),
    Neighbors = build_grid_neighbors(Height, Width, Grid),
    {Grid, Neighbors}.


build_lineofsight_grid(Rows) ->
    Height = length(Rows),
    Width = length(hd(Rows)),
    Grid = array:from_list(lists:append(Rows)),
    Neighbors = build_lineofsight_neighbors(Height, Width, Grid),
    {Grid, Neighbors}.


count_occupied_neighbors(Index, Grid, Neighbors) ->
    lists:foldl(
        fun (Neighbor, Acc) ->
            case array:get(Neighbor, Grid) of
                occupied -> Acc + 1;
                _ -> Acc
            end
        end, 0, array:get(Index, Neighbors)).


step(Grid, Neighbors, OccupiedThreshold) ->
    array:map(fun
        (_, floor) -> floor;
        (Index, empty) ->
            case count_occupied_neighbors(Index, Grid, Neighbors) of
                0 -> occupied;
                _ -> empty
            end;
        (Index, occupied) ->
            case count_occupied_neighbors(Index, Grid, Neighbors) of
                Count when Count >= OccupiedThreshold -> empty;
                _ -> occupied
            end
    end, Grid).


find_stable_state(Grid, Neighbors, OccupiedThreshold) ->
    case step(Grid, Neighbors, OccupiedThreshold) of
        Grid -> Grid;
        NextGrid -> find_stable_state(NextGrid, Neighbors, OccupiedThreshold)
    end.


count_occupied_seats(Grid) ->
    array:foldl(
        fun
            (_, occupied, Acc) -> Acc + 1;
            (_, _, Acc) -> Acc
        end, 0, Grid).


solve_part1(Input) ->
    {Grid, Neighbors} = build_grid(Input),
    FinalGrid = find_stable_state(Grid, Neighbors, 4),
    count_occupied_seats(FinalGrid).


solve_part2(Input) ->
    {Grid, Neighbors} = build_lineofsight_grid(Input),
    FinalGrid = find_stable_state(Grid, Neighbors, 5),
    count_occupied_seats(FinalGrid).


test_part1() ->
    Input = read_input("test.txt"),
    {Grid, Neighbors} = build_grid(Input),
    Step1 = step(Grid, Neighbors, 4),
    ?assertEqual(read_grid("part1-step1.txt"), Step1),
    Step2 = step(Step1, Neighbors, 4),
    ?assertEqual(read_grid("part1-step2.txt"), Step2),
    Step3 = step(Step2, Neighbors, 4),
    ?assertEqual(read_grid("part1-step3.txt"), Step3),
    Step4 = step(Step3, Neighbors, 4),
    ?assertEqual(read_grid("part1-step4.txt"), Step4),
    Step5 = step(Step4, Neighbors, 4),
    ?assertEqual(read_grid("part1-step5.txt"), Step5),
    ?assertEqual(Step5, step(Step5, Neighbors, 4)),
    ?assertEqual(37, solve_part1(Input)).


test_los() ->
    Los1 = read_input("lineofsight1.txt"),
    Height = length(Los1),
    Width = length(hd(Los1)),
    LosGrid1 = array:from_list(lists:append(Los1)),

    ?assertEqual(12, find_lineofsight_seat(0, -1, 3, 4, Height, Width, LosGrid1)),

    {LosGrid1, LosNeighbors1} = build_lineofsight_grid(Los1),
    ?assertEqual(empty, array:get(39, LosGrid1)),
    ?assertEqual([19,38,63,12,75,7,44,49], array:get(39, LosNeighbors1)),
    ?assertEqual(8, count_occupied_neighbors(39, LosGrid1, LosNeighbors1)),

    Los2 = read_input("lineofsight2.txt"),
    {LosGrid2, LosNeighbors2} = build_lineofsight_grid(Los2),

    ?assertEqual(empty, array:get(14, LosGrid2)),
    ?assertEqual([16], array:get(14, LosNeighbors2)),
    ?assertEqual(0, count_occupied_neighbors(14, LosGrid2, LosNeighbors2)),

    ?assertEqual(empty, array:get(16, LosGrid2)),
    ?assertEqual([14, 18], array:get(16, LosNeighbors2)),
    ?assertEqual(1, count_occupied_neighbors(16, LosGrid2, LosNeighbors2)),

    Los3 = read_input("lineofsight3.txt"),
    {LosGrid3, LosNeighbors3} = build_lineofsight_grid(Los3),

    ?assertEqual(empty, array:get(24, LosGrid3)),
    ?assertEqual([], array:get(24, LosNeighbors3)),
    ?assertEqual(0, count_occupied_neighbors(24, LosGrid3, LosNeighbors3)),

    ok.


test_part2() ->
    test_los(),

    Input = read_input("test.txt"),
    {Grid, Neighbors} = build_lineofsight_grid(Input),

    Step1 = step(Grid, Neighbors, 5),
    ?assertEqual(read_grid("part2-step1.txt"), Step1),
    Step2 = step(Step1, Neighbors, 5),
    ?assertEqual(read_grid("part2-step2.txt"), Step2),
    Step3 = step(Step2, Neighbors, 5),
    ?assertEqual(read_grid("part2-step3.txt"), Step3),
    Step4 = step(Step3, Neighbors, 5),
    ?assertEqual(read_grid("part2-step4.txt"), Step4),
    Step5 = step(Step4, Neighbors, 5),
    ?assertEqual(read_grid("part2-step5.txt"), Step5),
    Step6 = step(Step5, Neighbors, 5),
    ?assertEqual(read_grid("part2-step6.txt"), Step6),

    ?assertEqual(Step6, step(Step6, Neighbors, 5)),
    ?assertEqual(26, solve_part2(Input)).


test() ->
    test_part1(),
    test_part2().


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
