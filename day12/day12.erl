-module(day12).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [{Op, binary_to_integer(Arg)} || <<Op, Arg/binary>> <- Lines].


rotation_matrix(90) -> {0, -1, 1, 0};
rotation_matrix(180) -> {-1, 0, 0, -1};
rotation_matrix(270) -> {0, 1, -1, 0}.


turn(Amount, DX, DY) ->
    {A, B, C, D} = rotation_matrix(Amount),
    {(A * DX) + (B * DY), (C * DX) + (D * DY)}.


step_ship({Op, Arg}, {X, Y, DX, DY}) ->
    case Op of
        $N -> {X, Y + Arg, DX, DY};
        $S -> {X, Y - Arg, DX, DY};
        $E -> {X + Arg, Y, DX, DY};
        $W -> {X - Arg, Y, DX, DY};
        $F -> {X + (Arg * DX), Y + (Arg * DY), DX, DY};
        $L ->
            {NextDX, NextDY} = turn(Arg, DX, DY),
            {X, Y, NextDX, NextDY};
        $R ->
            {NextDX, NextDY} = turn(360 - Arg, DX, DY),
            {X, Y, NextDX, NextDY}
    end.


run_ship(Steps, InitialState) ->
    lists:foldl(fun step_ship/2, InitialState, Steps).


step_waypoint({Op, Arg}, {X, Y, DX, DY}) ->
    case Op of
        $N -> {X, Y, DX, DY + Arg};
        $S -> {X, Y, DX, DY - Arg};
        $E -> {X, Y, DX + Arg, DY};
        $W -> {X, Y, DX - Arg, DY};
        $F -> {X + (Arg * DX), Y + (Arg * DY), DX, DY};
        $L ->
            {NextDX, NextDY} = turn(Arg, DX, DY),
            {X, Y, NextDX, NextDY};
        $R ->
            {NextDX, NextDY} = turn(360 - Arg, DX, DY),
            {X, Y, NextDX, NextDY}
    end.


run_waypoint(Steps, InitialState) ->
    lists:foldl(fun step_waypoint/2, InitialState, Steps).


solve_part1(Input) ->
    {X, Y, _, _} = run_ship(Input, {0, 0, 1, 0}),
    abs(X) + abs(Y).


solve_part2(Input) ->
    {X, Y, _, _} = run_waypoint(Input, {0, 0, 10, 1}),
    abs(X) + abs(Y).


test() ->
    Input = read_input("test.txt"),

    ?assertEqual({10,0,1,0}, step_ship({$F, 10}, {0,0,1,0})),
    ?assertEqual({10,3,1,0}, step_ship({$N, 3}, {10,0,1,0})),
    ?assertEqual({17,3,1,0}, step_ship({$F, 7}, {10,3,1,0})),
    ?assertEqual({17,3,0,-1}, step_ship({$R, 90}, {17,3,1,0})),
    ?assertEqual({17,-8,0,-1}, step_ship({$F, 11}, {17,3,0,-1})),
    ?assertEqual(25, solve_part1(Input)),


    ?assertEqual({100,10,10,1}, step_waypoint({$F, 10}, {0,0,10,1})),
    ?assertEqual({100,10,10,4}, step_waypoint({$N, 3}, {100,10,10,1})),
    ?assertEqual({170,38,10,4}, step_waypoint({$F, 7}, {100,10,10,4})),
    ?assertEqual({170,38,4,-10}, step_waypoint({$R, 90}, {170,38,10,4})),
    ?assertEqual({214,-72,4,-10}, step_waypoint({$F, 11}, {170,38,4,-10})),
    ?assertEqual(286, solve_part2(Input)).


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
