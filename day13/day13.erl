-module(day13).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


parse_bus(<<"x">>) -> outofservice;
parse_bus(Value) -> binary_to_integer(Value).


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    [ETA, Line2] = binary:split(Data, <<"\n">>, [global, trim]),
    Buses = [parse_bus(Bus) || Bus <- binary:split(Line2, <<",">>, [global])],
    {binary_to_integer(ETA), Buses}.


get_wait(ETA, Bus) ->
    Bus * ((ETA div Bus) + 1) - ETA.


solve_part1(ETA, Buses) ->
    WorkingBuses = [{get_wait(ETA, Bus), Bus} || Bus <- Buses, Bus =/= outofservice],
    {Wait, Bus} = lists:min(WorkingBuses),
    Wait * Bus.


enumerate(List) ->
    lists:zip(lists:seq(0, length(List) - 1), List).


next_timestamp(N, Step, Index, Bus) ->
    case (N + Index) rem Bus of
        0 -> N;
        _ -> next_timestamp(N + Step, Step, Index, Bus)
    end.


solve_part2(ETA, Buses) ->
    {_, Timestamp} = lists:foldl(
        fun
            ({_, outofservice}, Acc) -> Acc;
            ({Index, Bus}, {Step, N}) ->
                    {Step * Bus, next_timestamp(N, Step, Index, Bus)}
        end, {1, ETA}, enumerate(Buses)),
    Timestamp.


test() ->
    {ETA, Buses} = read_input("test.txt"),
    ?assertEqual(295, solve_part1(ETA, Buses)),
    ?assertEqual(1068781, solve_part2(ETA, Buses)).


part1() ->
    {ETA, Buses} = read_input("input.txt"),
    solve_part1(ETA, Buses).


part2() ->
    {ETA, Buses} = read_input("input.txt"),
    solve_part2(ETA, Buses).
