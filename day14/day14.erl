-module(day14).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


parse_line(<<"mask = ", Mask/binary>>) -> {mask, binary_to_list(Mask)};
parse_line(<<"mem[", Data/binary>>) ->
    [Addr, Value] = binary:split(Data, <<"] = ">>),
    {mem, binary_to_integer(Addr), binary_to_integer(Value)}.


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [parse_line(Line) || Line <- Lines].


to_bin36(Value) ->
    lists:append(string:pad(integer_to_list(Value, 2), 36, leading, $0)).


combine($X, V) -> V;
combine(M, _) -> M.


apply_value_mask(Mask, Value) ->
    Bin = to_bin36(Value),
    Zip = lists:zip(Mask, Bin),
    Result = [combine(M, V) || {M, V} <- Zip],
    list_to_integer(Result, 2).


run_value_mask([], _Mask, Memory) -> Memory;
run_value_mask([Instruction | Program], Mask, Memory) ->
    case Instruction of
        {mask, NewMask} -> run_value_mask(Program, NewMask, Memory);
        {mem, Addr, Value} -> run_value_mask(Program, Mask, maps:put(Addr, apply_value_mask(Mask, Value), Memory))
    end.

run_value_mask(Program) ->
    run_value_mask(Program, none, #{}).


apply_addr_mask([], [], Masks) -> Masks;

apply_addr_mask([$X | Mask], [_ | Addr], Results) ->
    apply_addr_mask(Mask, Addr, [[$0 | R] || R <- Results] ++ [[$1 | R] || R <- Results]);

apply_addr_mask([$0 | Mask], [V | Addr], Results) ->
    apply_addr_mask(Mask, Addr, [[V | R] || R <- Results]);

apply_addr_mask([$1 | Mask], [_ | Addr], Results) ->
    apply_addr_mask(Mask, Addr, [[$1 | R] || R <- Results]).

apply_addr_mask(Mask, Addr) ->
    Results = apply_addr_mask(lists:reverse(Mask), lists:reverse(to_bin36(Addr)), [""]),
    [list_to_integer(Result, 2) || Result <- Results].


update_memory([], _Value, Memory) -> Memory;
update_memory([Addr | Tail], Value, Memory) ->
    update_memory(Tail, Value, maps:put(Addr, Value, Memory)).


run_addr_mask([], _Mask, Memory) -> Memory;
run_addr_mask([Instruction | Program], Mask, Memory) ->
    case Instruction of
        {mask, NewMask} -> run_addr_mask(Program, NewMask, Memory);
        {mem, Addr, Value} ->
            Addrs = apply_addr_mask(Mask, Addr),
            NewMemory = update_memory(Addrs, Value, Memory),
            run_addr_mask(Program, Mask, NewMemory)
    end.

run_addr_mask(Program) ->
    run_addr_mask(Program, [], #{}).


solve_part1(Input) ->
    Memory = run_value_mask(Input),
    lists:sum(maps:values(Memory)).


solve_part2(Input) ->
    Memory = run_addr_mask(Input),
    lists:sum(maps:values(Memory)).


test() ->
    Part1Input = read_input("test-part1.txt"),
    ?assertEqual(165, solve_part1(Part1Input)),
    Part2Input = read_input("test-part2.txt"),
    ?assertEqual(208, solve_part2(Part2Input)).


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
