-module(day15).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


setup([Last], Turn, Memory) ->
    {Turn + 1, Last, Memory};

setup([Head | Tail], Turn, Memory) ->
    setup(Tail, Turn + 1, maps:put(Head, Turn, Memory)).


next_number(never, _) -> 0;
next_number(LastTurn, CurrentTurn) -> CurrentTurn - LastTurn.


play(0, _Turn, Last, _Memory) -> Last;
play(Count, Turn, Last, Memory) ->
    Current = next_number(maps:get(Last, Memory, never), Turn - 1),
    % io:fwrite("~w ~w ~w ~w~n", [Turn, Last, Current, Memory]),
    NewMemory = maps:put(Last, Turn - 1, Memory),
    play(Count - 1, Turn + 1, Current, NewMemory).


solve(Setup, Count) ->
    {Turn, Last, Memory} = setup(Setup, 1, #{}),
    play(Count - Turn + 1, Turn, Last, Memory).


test() ->
    ?assertEqual(436, solve([0,3,6], 2020)),
    ?assertEqual(1, solve([1,3,2], 2020)),
    ?assertEqual(10, solve([2,1,3], 2020)),
    ?assertEqual(27, solve([1,2,3], 2020)),
    ?assertEqual(78, solve([2,3,1], 2020)),
    ?assertEqual(438, solve([3,2,1], 2020)),
    ?assertEqual(1836, solve([3,1,2], 2020)).


part1() ->
    solve([7,14,0,17,11,1,2], 2020).


part2() ->
    solve([7,14,0,17,11,1,2], 30000000).
