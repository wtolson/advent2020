-module(day16).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


parse_range(Range) ->
    [Start, Stop] = binary:split(Range, <<"-">>),
    {binary_to_integer(Start), binary_to_integer(Stop)}.


parse_rule(Line) ->
    [Name, Data] = binary:split(Line, <<": ">>),
    Ranges = binary:split(Data, <<" or ">>, [global, trim]),
    {Name, [parse_range(Range) || Range <- Ranges]}.


parse_rules(Data) ->
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [parse_rule(Line) || Line <- Lines].


parse_ticket(Data) ->
    Fields = binary:split(Data, <<",">>, [global, trim]),
    [binary_to_integer(Field) || Field <- Fields].


parse_your_ticket(Data) ->
    [_, Ticket] = binary:split(Data, <<"\n">>, [global, trim]),
    parse_ticket(Ticket).


parse_nearby_tickets(Data) ->
    Tickets = tl(binary:split(Data, <<"\n">>, [global, trim])),
    [parse_ticket(Ticket) || Ticket <- Tickets].


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    [Rules, YourTicket, NearbyTickets] = binary:split(Data, <<"\n\n">>, [global, trim]),
    {parse_rules(Rules), parse_your_ticket(YourTicket), parse_nearby_tickets(NearbyTickets)}.


flatten_rules([], Acc) -> Acc;

flatten_rules([{_, Ranges} | Rules], Acc) ->
    flatten_rules(Rules, Acc ++ Ranges).

flatten_rules(Rules) ->
    flatten_rules(Rules, []).


is_valid_field([], _) -> false;
is_valid_field([{Start, Stop} | _], Value) when Value >= Start, Value =< Stop -> true;
is_valid_field([_ | Rules], Value) -> is_valid_field(Rules, Value).


sum_invalid_fields(_, [], Acc) -> Acc;

sum_invalid_fields(Rules, [Value | Tail], Acc) ->
    case is_valid_field(Rules, Value) of
        true -> sum_invalid_fields(Rules, Tail, Acc);
        false -> sum_invalid_fields(Rules, Tail, Acc + Value)
    end.

sum_invalid_fields(Rules, Tickets) ->
    sum_invalid_fields(Rules, Tickets, 0).


sum_invalid_tickets(_, [], Acc) -> Acc;

sum_invalid_tickets(Rules, [Ticket | Tail], Acc) ->
    sum_invalid_tickets(Rules, Tail, Acc + sum_invalid_fields(Rules, Ticket)).

sum_invalid_tickets(Rules, Tickets) ->
    sum_invalid_tickets(flatten_rules(Rules), Tickets, 0).


solve_part1({Rules, _, Tickets}) ->
    sum_invalid_tickets(Rules, Tickets).


is_valid_ticket(_, []) -> true;

is_valid_ticket(Rules, [Head | Tail]) ->
    case is_valid_field(Rules, Head) of
        false -> false;
        true -> is_valid_ticket(Rules, Tail)
    end.


filter_fields(_, [], [], Acc) -> Acc;

filter_fields(Rules, [Index | Indexes], [Field | Fields], Acc) ->
    NextAcc = maps:map(
        fun (Name, Possibilities) ->
            Ranges = maps:get(Name, Rules),
            case is_valid_field(Ranges, Field) of
                true -> Possibilities;
                false -> ordsets:del_element(Index, Possibilities)
            end
        end, Acc),
    filter_fields(Rules, Indexes, Fields, NextAcc).

filter_fields(Rules, Ticket, Acc) ->
    filter_fields(Rules, lists:seq(1, length(Ticket)), Ticket, Acc).


eliminate_fields(Fields) ->
    NewFields = maps:fold(fun
        (A, [Index], Acc) ->
            maps:map(fun
                (B, Possibilities) when A == B -> Possibilities;
                (_, Possibilities) -> ordsets:del_element(Index, Possibilities)
            end, Acc);
        (_, _, Acc) -> Acc
    end, Fields, Fields),

    case NewFields == Fields of
        true -> [{Name, Value} || {Name, [Value]} <- maps:to_list(Fields)];
        false -> eliminate_fields(NewFields)
    end.


detect_fields(_, [], Acc) ->
    eliminate_fields(Acc);

detect_fields(Rules, [Ticket | Tail], Acc) ->
    detect_fields(Rules, Tail, filter_fields(Rules, Ticket, Acc)).

detect_fields(Rules, ValidTickets) ->
    NumFields = length(hd(ValidTickets)),
    Possibilities = maps:from_list(
        [{Name, ordsets:from_list(lists:seq(1, NumFields))} || {Name, _} <- Rules]),
    detect_fields(maps:from_list(Rules), ValidTickets, Possibilities).


multiply_departure_fields([], _, Acc) -> Acc;

multiply_departure_fields([{<<"departure ", _/binary>>, Index} | Fields], Ticket, Acc) ->
    multiply_departure_fields(Fields, Ticket, Acc * lists:nth(Index, Ticket));

multiply_departure_fields([_ | Fields], Ticket, Acc) ->
    multiply_departure_fields(Fields, Ticket, Acc).

multiply_departure_fields(Fields, Ticket) ->
    multiply_departure_fields(Fields, Ticket, 1).


solve_part2({Rules, YourTicket, NearbyTickets}) ->
    Ranges = flatten_rules(Rules),
    ValidTickets = lists:filter(
        fun (Ticket) -> is_valid_ticket(Ranges, Ticket) end, NearbyTickets),
    Fields = detect_fields(Rules, ValidTickets),
    multiply_departure_fields(Fields, YourTicket).


test() ->
    Part1Input = read_input("test-part1.txt"),
    ?assertEqual(71, solve_part1(Part1Input)),

    {Rules, _, Tickets} = read_input("test-part2.txt"),
    ?assertEqual([{<<"class">>,2},{<<"row">>,1},{<<"seat">>,3}], detect_fields(Rules, Tickets)).


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
