-module(day17).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


new_pos(3, Row, Col) -> {0, Col, Row};
new_pos(4, Row, Col) -> {0, 0, Col, Row}.


parse_line([], _, _, _, Output) -> Output;
parse_line([$. | Tail], Dim, Row, Col, Output) ->
    parse_line(Tail, Dim, Row, Col+1, Output);
parse_line([$# | Tail], Dim, Row, Col, Output) ->
    parse_line(Tail, Dim, Row, Col+1, maps:put(new_pos(Dim, Row, Col), true, Output)).


parse_input([], _, _, Output) -> Output;
parse_input([Line | Tail], Dim, Row, Output) ->
    parse_input(Tail, Dim, Row+1, parse_line(binary_to_list(Line), Dim, Row, 0, Output)).


read_input(Filename, Dim) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    parse_input(Lines, Dim, 0, #{}).


neighbors({X, Y, Z}) ->
    [{X + DX, Y + DY, Z + DZ} ||
        DX <- [-1, 0, 1],
        DY <- [-1, 0, 1],
        DZ <- [-1, 0, 1],
        {DX, DY, DZ} /= {0, 0, 0}];

neighbors({X, Y, Z, W}) ->
    [{X + DX, Y + DY, Z + DZ, W + DW} ||
        DX <- [-1, 0, 1],
        DY <- [-1, 0, 1],
        DZ <- [-1, 0, 1],
        DW <- [-1, 0, 1],
        {DX, DY, DZ, DW} /= {0, 0, 0, 0}].


total_active_neighbors(Pos, Grid) ->
    lists:foldl(
        fun (Neighbor, Acc) ->
            case maps:get(Neighbor, Grid, false) of
                true -> Acc + 1;
                false -> Acc
            end
        end, 0, neighbors(Pos)).


step_active(Grid, Output) ->
    maps:fold(
        fun (Pos, _, Acc) ->
            case total_active_neighbors(Pos, Grid) of
                2 -> maps:put(Pos, true, Acc);
                3 -> maps:put(Pos, true, Acc);
                _ -> Acc
            end
        end, Output, Grid).


inactive_neighbors(Grid) ->
    maps:fold(
        fun (Pos, _, Acc) ->
            Neighbors = lists:filter(
                fun (Neighbor) -> not maps:get(Neighbor, Grid, false) end,
                neighbors(Pos)),
            sets:union(Acc, sets:from_list(Neighbors))
        end, sets:new(), Grid).


step_inactive(Grid, Output) ->
    sets:fold(
        fun (Pos, Acc) ->
            case total_active_neighbors(Pos, Grid) of
                3 -> maps:put(Pos, true, Acc);
                _ -> Acc
            end
        end, Output, inactive_neighbors(Grid)).


step(Grid) ->
    step_inactive(Grid, step_active(Grid, #{})).


run(0, Grid) -> Grid;
run(Steps, Grid) ->
    run(Steps-1, step(Grid)).


solve(Input) ->
    Output = run(6, Input),
    maps:size(Output).


test() ->
    Input3 = read_input("test.txt", 3),
    ?assertEqual(112, solve(Input3)),
    Input4 = read_input("test.txt", 4),
    ?assertEqual(848, solve(Input4)).


part1() ->
    Input = read_input("input.txt", 3),
    solve(Input).


part2() ->
    Input = read_input("input.txt", 4),
    solve(Input).
