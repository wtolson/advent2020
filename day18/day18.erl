-module(day18).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [binary_to_list(Line) || Line <- Lines].


parse({integer, _, Value}) -> {integer, Value};
parse({Token, _}) -> Token;
parse(String) ->
    {ok, Tokens, _} = erl_scan:string(String),
    [parse(Token) || Token <- Tokens].


eval([], [{integer, Value}]) -> Value;

eval([Token | Tokens], Stack) ->
    case Token of
        '+' -> eval(Tokens, ['+' | Stack]);
        '*' -> eval(Tokens, ['*' | Stack]);
        '(' -> eval(Tokens, ['(' | Stack]);
        ')' ->
            [{integer, Value}, '(' | StackTail] = Stack,
            eval([{integer, Value} | Tokens], StackTail);
        {integer, A} ->
            case Stack of
                ['+', {integer, B} | StackTail] -> eval(Tokens, [{integer, A + B} | StackTail]);
                ['*', {integer, B} | StackTail] -> eval(Tokens, [{integer, A * B} | StackTail]);
                _ -> eval(Tokens, [{integer, A} | Stack])
            end
    end.

eval(String) ->
    eval(parse(String), []).


eval_adv([], [{integer, Value}]) -> Value;

eval_adv([], [{integer, Value}, '(' | StackTail]) -> {Value, StackTail};

eval_adv([], [{integer, A}, '*', {integer, B} | StackTail]) ->
    eval_adv([], [{integer, A * B} | StackTail]);

eval_adv([Token | Tokens], Stack) ->
    case Token of
        '+' -> eval_adv(Tokens, ['+' | Stack]);
        '*' -> eval_adv(Tokens, ['*' | Stack]);
        '(' -> eval_adv(Tokens, ['(' | Stack]);
        ')' ->
            {Value, StackTail} = eval_adv([], Stack),
            eval_adv([{integer, Value} | Tokens], StackTail);
        {integer, A} ->
            case Stack of
                ['+', {integer, B} | StackTail] -> eval_adv(Tokens, [{integer, A + B} | StackTail]);
                _ -> eval_adv(Tokens, [{integer, A} | Stack])
            end
    end.

eval_adv(String) ->
    eval_adv(parse(String), []).


solve_part1(Input) ->
    lists:sum([eval(Line) || Line <- Input]).


solve_part2(Input) ->
    lists:sum([eval_adv(Line) || Line <- Input]).


test() ->
    ?assertEqual(71, eval("1 + 2 * 3 + 4 * 5 + 6")),
    ?assertEqual(51, eval("1 + (2 * 3) + (4 * (5 + 6))")),
    ?assertEqual(26, eval("2 * 3 + (4 * 5)")),
    ?assertEqual(437, eval("5 + (8 * 3 + 9 + 3 * 4 * 3)")),
    ?assertEqual(12240, eval("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")),
    ?assertEqual(13632, eval("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")),

    ?assertEqual(231, eval_adv("1 + 2 * 3 + 4 * 5 + 6")),
    ?assertEqual(51, eval_adv("1 + (2 * 3) + (4 * (5 + 6))")),
    ?assertEqual(46, eval_adv("2 * 3 + (4 * 5)")),
    ?assertEqual(1445, eval_adv("5 + (8 * 3 + 9 + 3 * 4 * 3)")),
    ?assertEqual(669060, eval_adv("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")),
    ?assertEqual(23340, eval_adv("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")),

    ok.


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
