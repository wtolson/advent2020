-module(day19).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


parse_group(Data) ->
    Parts = binary:split(Data, <<" ">>, [global, trim]),
    {group, [binary_to_integer(Part) || Part <- Parts]}.


parse_rule_value(<<$", Char, $">>) -> {char, Char};
parse_rule_value(Data) ->
    case binary:split(Data, <<" | ">>, [global, trim]) of
        [Group1, Group2] -> {split, parse_group(Group1), parse_group(Group2)};
        [Group] -> parse_group(Group)
    end.


parse_rule(Data) ->
    [Index, Value] = binary:split(Data, <<": ">>),
    {binary_to_integer(Index), parse_rule_value(Value)}.


parse_rules(Data) ->
    Rules = binary:split(Data, <<"\n">>, [global, trim]),
    maps:from_list([parse_rule(Rule) || Rule <- Rules]).


parse_messages(Data) ->
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [binary_to_list(Line) || Line <- Lines].


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    [Rules, Messages] = binary:split(Data, <<"\n\n">>, [global, trim]),
    {parse_rules(Rules), parse_messages(Messages)}.


match(_Rules, {char, Char}, [Char], [], _Threads) ->
    true;

match(Rules, {char, Char}, [Char | StringTail], [StackHead | StackTail], Threads) ->
    match(Rules, maps:get(StackHead, Rules), StringTail, StackTail, Threads);

match(_Rules, {char, _Char}, _String, _Stack, []) ->
    false;

match(Rules, {char, _Char}, _String, _Stack, [{Rule, String, Stack} | Threads]) ->
    match(Rules, Rule, String, Stack, Threads);

match(Rules, {group, [Index | Group]}, String, Stack, Threads) ->
    match(Rules, maps:get(Index, Rules), String, Group ++ Stack, Threads);

match(Rules, {split, Rule1, Rule2}, String, Stack, Threads) ->
    match(Rules, Rule1, String, Stack, [{Rule2, String, Stack} | Threads]).

match(Rules, String) ->
    match(Rules, maps:get(0, Rules), String, [], []).


solve_part1({Rules, Messages}) ->
    lists:foldl(
        fun (Message, Acc) ->
            case match(Rules, Message) of
                true -> Acc + 1;
                false -> Acc
            end
        end, 0, Messages).


solve_part2({Rules, Messages}) ->
    NewRules = maps:merge(
        Rules, day19:parse_rules(<<"8: 42 | 42 8\n11: 42 31 | 42 11 31">>)),
    lists:foldl(
        fun (Message, Acc) ->
            case match(NewRules, Message) of
                true -> Acc + 1;
                false -> Acc
            end
        end, 0, Messages).


test() ->
    Input1 = read_input("test-part1.txt"),
    ?assertEqual(2, solve_part1(Input1)),
    Input2 = read_input("test-part2.txt"),
    ?assertEqual(12, solve_part2(Input2)).


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
