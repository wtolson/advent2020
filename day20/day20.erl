-module(day20).
-export([test/0, part1/0, part2/0, print_grid/1]).
-include_lib("stdlib/include/assert.hrl").


parse_tile(Data) ->
    [Label | Grid] = binary:split(Data, <<"\n">>, [global, trim]),
    <<"Tile ", Id:4/binary, ":">>  = Label,
    {binary_to_integer(Id), Grid}.


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Tiles = binary:split(Data, <<"\n\n">>, [global, trim]),
    [parse_tile(Tile) || Tile <- Tiles].


reverse(Binary) when is_binary(Binary) ->
    Size = erlang:size(Binary) * 8,
    <<X:Size/integer-little>> = Binary,
    <<X:Size/integer-big>>.


print_grid(Grid) ->
    lists:foreach(fun (Row) -> io:fwrite("~s~n", [Row]) end, Grid).


edge(top, Grid) ->
    hd(Grid);
edge(bottom, Grid) ->
    lists:last(Grid);
edge(left, Grid) ->
    << Pixel || <<Pixel:1/binary, _Rest:9/binary>> <- Grid >>;
edge(right, Grid) ->
    << Pixel || <<_Rest:9/binary, Pixel:1/binary>> <- Grid >>.


add_edges({Id, Grid}, EdgeMap) ->
    Edges = [edge(Direction, Grid) || Direction <- [top, bottom, right, left]],
    ReversedEdges = [reverse(Edge) || Edge <- Edges],

    lists:foldl(
        fun (Edge, Acc) ->
            maps:update_with(
                Edge, fun (Ids) -> ordsets:add_element(Id, Ids) end, [Id], Acc)
        end, EdgeMap, Edges ++ ReversedEdges).


make_edge_map(Tiles) ->
    lists:foldl(fun add_edges/2, #{}, Tiles).


count_sides(EdgeMap) ->
    maps:fold(
        fun
            (_Edge, [Id], Acc) ->
                maps:update_with(Id, fun (Count) -> Count + 1 end, 1, Acc);
            (_Edge, _Ids, Acc) -> Acc
        end, #{}, EdgeMap).


find_corners(Tiles) ->
    EdgeMap = make_edge_map(Tiles),
    SideCount = count_sides(EdgeMap),
    maps:fold(
        fun
            (Id, 4, Acc) -> [Id | Acc];
            (_Id, _Count, Acc) -> Acc
        end, [], SideCount).


opposite_direction(top) -> bottom;
opposite_direction(bottom) -> top;
opposite_direction(left) -> right;
opposite_direction(right) -> left.


neighbor(top, {X, Y}) ->{X, Y - 1};
neighbor(bottom, {X, Y}) ->{X, Y + 1};
neighbor(left, {X, Y}) ->{X - 1, Y};
neighbor(right, {X, Y}) ->{X + 1, Y}.


neighbors(Pos) ->
    [neighbor(Direction, Pos) || Direction <- [top, bottom, left, right]].


empty_neighbors(Pos, Image) ->
    [Neighbor || Neighbor <- neighbors(Pos), not maps:is_key(Neighbor, Image)].


neighbor_edges(Pos, Image) ->
    lists:foldl(
        fun (Direction, Acc) ->
            case maps:get(neighbor(Direction, Pos), Image, empty) of
                empty -> Acc;
                Grid ->
                    [{Direction, edge(opposite_direction(Direction), Grid)} | Acc]
            end
        end, [], [top, bottom, left, right]).


rotate([<<>> | _], Acc) -> Acc;
rotate(Grid, Acc) ->
    Row = << Head || <<Head:1/binary, _Tail/binary>> <- Grid >>,
    Remaining = [Tail || <<_Head:1/binary, Tail/binary>> <- Grid],
    rotate(Remaining, [Row | Acc]).


rotate(Grid) ->
    rotate(Grid, []).


flip(Grid) ->
    [reverse(Row) || Row <- Grid].


is_oriented(_Grid, []) -> true;
is_oriented(Grid, [{Direction, Edge} | Edges]) ->
    case edge(Direction, Grid) of
        Edge -> is_oriented(Grid, Edges);
        _ -> false
    end.


orient(_Grid, _Edges, 0, false) -> error;

orient(Grid, Edges, 0, true) ->
    orient(flip(Grid), Edges, 4, false);

orient(Grid, Edges, Rotations, Flip) ->
    case is_oriented(Grid, Edges) of
        true -> Grid;
        false -> orient(rotate(Grid), Edges, Rotations - 1, Flip)
    end.

orient(Grid, Edges) ->
    orient(Grid, Edges, 4, true).


find_matches(EdgeMap, [{_Direction, Edge}]) ->
    maps:get(Edge, EdgeMap, []);

find_matches(EdgeMap, [{_Direction, Edge1}, Edge2 | Edges]) ->
    case maps:get(Edge1, EdgeMap, none) of
        none -> find_matches(EdgeMap, [Edge2 | Edges]);
        Matches ->
            ordsets:intersection(Matches, find_matches(EdgeMap, [Edge2 | Edges]))
    end.


bbox(Image) ->
    lists:foldl(
        fun ({X, Y}, {MinX, MinY, MaxX, MaxY}) ->
            {min(MinX, X), min(MinY, Y), max(MaxX, X), max(MaxY, Y)}
        end, {0, 0, 0, 0}, maps:keys(Image)).


trim_edges(Row) when is_binary(Row) ->
    Size = size(Row) - 2,
    <<_First:1/binary, Middle:Size/binary, _Last:1/binary>> = Row,
    Middle;

trim_edges(Grid) when is_list(Grid) ->
    Middle = lists:droplast(tl(Grid)),
    [trim_edges(Row) || Row <- Middle].

combine_row([], Acc) -> Acc;
combine_row([Head | Tail], Acc) ->
    combine_row(Tail, [<<A/binary, B/binary>> || {A, B} <- lists:zip(Acc, Head)]).

combine_row(Y, MinX, MaxX, Image) ->
    [Head | Tail] = [trim_edges(maps:get({X, Y}, Image)) || X <- lists:seq(MinX, MaxX)],
    combine_row(Tail, Head).

combine_image(Image) ->
    {MinX, MinY, MaxX, MaxY} = bbox(Image),
    lists:flatmap(
        fun (Y) -> combine_row(Y, MinX, MaxX, Image) end, lists:seq(MinY, MaxY)).


place_tile(Pos, Id, Grid, Image, EdgeMap, TileMap, Used, Frontier) ->
    NextImage = maps:put(Pos, Grid, Image),
    NextTileMap = maps:remove(Id, TileMap),
    NextUsed = ordsets:add_element(Id, Used),
    NextFrontier = ordsets:union(
        Frontier, ordsets:from_list(empty_neighbors(Pos, NextImage))),
    reassemble_image(NextImage, EdgeMap, NextTileMap, NextUsed, NextFrontier).


reassemble_image(Image, _EdgeMap, TileMap, _Used, []) ->
    case maps:size(TileMap) of
        0 -> combine_image(Image);
        _ -> error
    end;

reassemble_image(Image, EdgeMap, TileMap, Used, [Pos | Frontier]) ->
    Edges = neighbor_edges(Pos, Image),
    Matches = ordsets:subtract(find_matches(EdgeMap, Edges), Used),

    case Matches of
        [] ->
            reassemble_image(Image, EdgeMap, TileMap, Used, Frontier);
        [Id] ->
            case orient(maps:get(Id, TileMap), Edges) of
                error ->
                    reassemble_image(Image, EdgeMap, TileMap, Used, Frontier);
                Tile ->
                    case place_tile(Pos, Id, Tile, Image, EdgeMap, TileMap, Used, Frontier) of
                        error ->
                            reassemble_image(Image, EdgeMap, TileMap, Used, Frontier);
                        Result -> Result
                    end
            end;
        _ ->
            reassemble_image(Image, EdgeMap, TileMap, Used, Frontier ++ [Pos])
    end.


reassemble_image(Tiles) ->
    {InitId, InitGrid} = hd(Tiles),
    TileMap = maps:from_list(Tiles),
    EdgeMap = make_edge_map(Tiles),
    place_tile({0, 0}, InitId, InitGrid, #{}, EdgeMap, TileMap, [], []).


pos_to_index(Cols, Col, Row) ->
    Col + (Cols * Row).


is_monster([], _Pos, _Cols, _Cells) -> true;
is_monster([{RowOffset, ColOffset} | Pattern], {Row, Col}, Cols, Cells) ->
    Index = pos_to_index(Cols, Col + ColOffset, Row + RowOffset),
    case array:get(Index, Cells) of
        $. -> false;
        _ -> is_monster(Pattern, {Row, Col}, Cols, Cells)
    end.


mark_monster([], _Pos, _Cols, Cells) -> Cells;
mark_monster([{RowOffset, ColOffset} | Pattern], {Row, Col}, Cols, Cells) ->
    Index = pos_to_index(Cols, Col + ColOffset, Row + RowOffset),
    NextCells = array:set(Index, $O, Cells),
    mark_monster(Pattern, {Row, Col}, Cols, NextCells).


count_non_monsters(Grid) ->
    %            1111111111
    %  01234567890123456789
    % 0                  #
    % 1#    ##    ##    ###
    % 2 #  #  #  #  #  #
    Pattern = [
        {0, 18}, {1, 0}, {1, 5}, {1, 6}, {1, 11}, {1, 12}, {1, 17}, {1, 18},
        {1, 19}, {2, 1}, {2, 4}, {2, 7}, {2, 10}, {2, 13}, {2, 16}],

    {Rows, Cols} = {length(Grid), size(hd(Grid))},
    Cells = array:from_list(lists:flatmap(fun erlang:binary_to_list/1, Grid)),

    SearchSpace = [{Row, Col} || Row <- lists:seq(0, Rows - 3), Col <- lists:seq(0, Cols - 20)],
    Result = lists:foldl(
        fun (Pos, Acc) ->
            case is_monster(Pattern, Pos, Cols, Acc) of
                false -> Acc;
                true -> mark_monster(Pattern, Pos, Cols, Acc)
            end
        end, Cells, SearchSpace),

    array:foldl(fun
            (_Index, $#, Acc) -> Acc + 1;
            (_Index, _, Acc) -> Acc
        end, 0, Result).


min_non_monster_count(_Grid, 0, false, Acc) -> Acc;

min_non_monster_count(Grid, 0, true, Acc) ->
    min_non_monster_count(flip(Grid), 4, false, Acc);

min_non_monster_count(Grid, Rotations, Flip, Acc) ->
    min_non_monster_count(rotate(Grid), Rotations - 1, Flip, min(Acc, count_non_monsters(Grid))).

min_non_monster_count(Grid) ->
    {Rows, Cols} = {length(Grid), size(hd(Grid))},
    min_non_monster_count(Grid, 4, true, Rows * Cols).


solve_part1(Input) ->
    Corners = find_corners(Input),
    lists:foldl(fun (Id, Acc) -> Id * Acc end, 1, Corners).


solve_part2(Input) ->
    Image = reassemble_image(Input),
    min_non_monster_count(Image).


test() ->
    Input = read_input("test.txt"),
    ?assertEqual([3079,2971,1951,1171], find_corners(Input)),
    ?assertEqual(20899048083289, solve_part1(Input)),
    ?assertEqual(273, solve_part2(Input)).


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).

