-module(day21).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


parse_ingredients(Data) ->
    binary:split(Data, <<" ">>, [global, trim]).


parse_allergens(Data) ->
    % Trim the final ')'
    Size = size(Data) - 1,
    <<Allergens:Size/binary, ")">> = Data,
    % Split the list
    binary:split(Allergens, <<", ">>, [global, trim]).


parse_line(Line) ->
    [Ingredients, Allergens] = binary:split(Line, <<" (contains ">>),
    {parse_ingredients(Ingredients), parse_allergens(Allergens)}.


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Lines = binary:split(Data, <<"\n">>, [global, trim]),
    [parse_line(Line) || Line <- Lines].


count_ingredients([], Counts) -> Counts;
count_ingredients([{Ingredients, _Allergens} | Foods], Counts) ->
    Result = lists:foldl(
        fun (Ingredient, Acc) ->
            Count = maps:get(Ingredient, Acc, 0),
            Acc#{ Ingredient => Count + 1 }
        end, Counts, Ingredients),
    count_ingredients(Foods, Result).

count_ingredients(Foods) ->
    count_ingredients(Foods, #{}).


all_ingredients([], IngredientSet) -> IngredientSet;
all_ingredients([{Ingredients, _Allergens} | Foods], IngredientSet) ->
    all_ingredients(Foods, ordsets:union(IngredientSet, ordsets:from_list(Ingredients))).

all_ingredients(Foods) ->
    all_ingredients(Foods, ordsets:new()).


find_possible_allergins([], AllergenSets) -> AllergenSets;
find_possible_allergins([{Ingredients, Allergens} | Foods], AllergenSets) ->
    IngredientSet = ordsets:from_list(Ingredients),
    Result = lists:foldl(
        fun (Allergen, Acc) ->
            case maps:get(Allergen, Acc, none) of
                none -> Acc#{ Allergen => IngredientSet };
                CurrentSet -> Acc#{ Allergen => ordsets:intersection(IngredientSet, CurrentSet) }
            end
        end, AllergenSets, Allergens),
    find_possible_allergins(Foods, Result).

find_possible_allergins(Foods) ->
    find_possible_allergins(Foods, #{}).


find_safe_ingredients(Foods) ->
    AllIngredients = all_ingredients(Foods),
    PossibleAllergins = find_possible_allergins(Foods),
    maps:fold(
        fun (_Allergen, Ingredients, Acc) ->
            ordsets:subtract(Acc, Ingredients)
        end, AllIngredients, PossibleAllergins).


eliminate_ingredients(PossibleAllergins) ->
    Result = maps:fold(fun
        (A, [Ingredient], Acc) ->
            maps:map(fun
                (B, Possibilities) when A == B -> Possibilities;
                (_, Possibilities) -> ordsets:del_element(Ingredient, Possibilities)
            end, Acc);
        (_, _, Acc) -> Acc
    end, PossibleAllergins, PossibleAllergins),

    case Result == PossibleAllergins of
        true -> [{Allergin, Value} ||
                    {Allergin, [Value]} <- maps:to_list(PossibleAllergins)];
        false -> eliminate_ingredients(Result)
    end.


identify_allergins(Foods) ->
    PossibleAllergins = find_possible_allergins(Foods),
    eliminate_ingredients(PossibleAllergins).


solve_part1(Input) ->
    Counts = count_ingredients(Input),
    Safe = find_safe_ingredients(Input),
    lists:sum(maps:values(maps:with(ordsets:to_list(Safe), Counts))).


solve_part2(Input) ->
    Allergins = identify_allergins(Input),
    SortedAllergins = lists:sort(Allergins),
    string:join([binary_to_list(Ingredient) ||
                    {_Allergin, Ingredient} <- SortedAllergins], ",").


test() ->
    Input = read_input("test.txt"),
    ?assertEqual(5, solve_part1(Input)),
    ?assertEqual("mxmxvkd,sqjhc,fvjkl", solve_part2(Input)).


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
