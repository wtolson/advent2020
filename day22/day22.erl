-module(day22).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


parse_player(Data) ->
    Lines = tl(binary:split(Data, <<"\n">>, [global, trim])),
    [binary_to_integer(Line) || Line <- Lines].


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    [Player1, Player2] = binary:split(Data, <<"\n\n">>, [global, trim]),
    {parse_player(Player1), parse_player(Player2)}.


play(Player1, []) ->
    {Player1, []};

play([], Player2) ->
    {[], Player2};

play([A | Player1], [B | Player2]) when A > B ->
    play(Player1 ++ [A, B], Player2);

play([A | Player1], [B | Player2]) when A < B ->
    play(Player1, Player2 ++ [B, A]).


select_winning_deck({Player1, []}) ->
    Player1;

select_winning_deck({[], Player2}) ->
    Player2.


enumerated_foldl(_Fun, Acc, [], _Start, _Incr) -> Acc;
enumerated_foldl(Fun, Acc, [Elem | Tail], Start, Incr) ->
    enumerated_foldl(Fun, Fun(Start, Elem, Acc), Tail, Start + Incr, Incr).


add_score(Index, Card, Acc) ->
    Acc + (Index * Card).


score(Deck) ->
    enumerated_foldl(fun add_score/3, 0, Deck, length(Deck), -1).


winner(A, Player1, B, Player2) when length(Player1) >= A, length(Player2) >= B ->
    case play_recursive(lists:sublist(Player1, A), lists:sublist(Player2, B)) of
        {_Player1, []} -> player1;
        {[], _Player2} -> player2
    end;

winner(A, _Player1, B, _Player2) when A > B ->
    player1;

winner(A, _Player1, B, _Player2) when A < B ->
    player2.


recursive_combat([A | Player1], [B | Player2], History) ->
    case winner(A, Player1, B, Player2) of
        player1 -> play_recursive(Player1 ++ [A, B], Player2, History);
        player2 -> play_recursive(Player1, Player2 ++ [B, A], History)
    end.


play_recursive(Player1, [], _History) ->
    {Player1, []};

play_recursive([], Player2, _History) ->
    {[], Player2};

play_recursive(Player1, Player2, History) ->
    State = {Player1, Player2},
    case sets:is_element(State, History) of
        true -> {Player1, []};
        false ->
            NextHistory = sets:add_element(State, History),
            recursive_combat(Player1, Player2, NextHistory)
    end.


play_recursive(Player1, Player2) ->
    play_recursive(Player1, Player2, sets:new()).


solve_part1({Player1, Player2}) ->
    score(select_winning_deck(play(Player1, Player2))).


solve_part2({Player1, Player2}) ->
    score(select_winning_deck(play_recursive(Player1, Player2))).


test() ->
    Input1 = read_input("test-part1.txt"),
    ?assertEqual(306, solve_part1(Input1)),
    ?assertEqual(291, solve_part2(Input1)),
    Input2 = read_input("test-part2.txt"),
    ?assertEqual(105, solve_part2(Input2)),
    ok.


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
