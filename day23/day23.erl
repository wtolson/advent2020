-module(day23).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


load(Input) ->
    [ I - 48 || I <- Input ].


dump(_Circle, [1 | Acc]) -> Acc;
dump(Circle, [Current | _] = Acc) ->
    #{ Current := Next } = Circle,
    dump(Circle, [Next | Acc]).

dump(Circle) ->
    #{ 1 := Next } = Circle,
    Result = dump(Circle, [Next]),
    [ R + 48 || R <- lists:reverse(Result) ].


circle_from_list([End], Circle, Start) ->
    Circle#{ End => Start};

circle_from_list([Elem1, Elem2 | Tail], Circle, Start) ->
    circle_from_list([Elem2 | Tail], Circle#{ Elem1 => Elem2}, Start).

circle_from_list([Head | _Tail] = List) ->
    circle_from_list(List, #{}, Head).


find_destination(Total, 0, A, B, C) ->
    find_destination(Total, Total, A, B, C);

find_destination(Total, Current, A, B, C) when Current == A; Current == B; Current == C ->
    find_destination(Total, Current - 1, A, B, C);

find_destination(_Total, Current, _, _, _) -> Current.


play(_Total, Circle, _Current, 0) -> Circle;
play(Total, Circle, Current, Rounds) ->
    #{Current := A} = Circle,
    #{A := B} = Circle,
    #{B := C} = Circle,
    #{C := Next} = Circle,

    Destination = find_destination(Total, Current - 1, A, B, C),
    #{Destination := Tail} = Circle,

    NextCircle = Circle#{ Current := Next, Destination := A, C := Tail },
    play(Total, NextCircle, Next, Rounds - 1).


solve_part1(Input, Rounds) ->
    [Current | _] = List = load(Input),
    Circle = circle_from_list(List),
    Total = length(List),
    dump(play(Total, Circle, Current, Rounds)).


solve_part2(Input) ->
    [Current | _] = List = load(Input),
    Circle = circle_from_list(List ++ lists:seq(10, 1000000)),
    Result = play(1000000, Circle, Current, 10000000),
    #{ 1 := A } = Result,
    #{ A := B } = Result,
    A * B.


test() ->
    ?assertEqual("92658374", solve_part1("389125467", 10)),
    ?assertEqual("67384529", solve_part1("389125467", 100)).


part1() ->
    solve_part1("463528179", 100).


part2() ->
    solve_part2("463528179").
