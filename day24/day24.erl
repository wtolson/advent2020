-module(day24).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


split(Subject, nl) -> split(Subject, <<"\n">>);
split(Subject, Pattern) ->
    binary:split(Subject, Pattern, [global, trim]).


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    [Line || Line <- split(Data, nl)].


parse_instructions(<<>>, Acc) -> lists:reverse(Acc);
parse_instructions(<<"se", Tail/binary>>, Acc) -> parse_instructions(Tail, [se | Acc]);
parse_instructions(<<"sw", Tail/binary>>, Acc) -> parse_instructions(Tail, [sw | Acc]);
parse_instructions(<<"ne", Tail/binary>>, Acc) -> parse_instructions(Tail, [ne | Acc]);
parse_instructions(<<"nw", Tail/binary>>, Acc) -> parse_instructions(Tail, [nw | Acc]);
parse_instructions(<<"e", Tail/binary>>, Acc) -> parse_instructions(Tail, [e | Acc]);
parse_instructions(<<"w", Tail/binary>>, Acc) -> parse_instructions(Tail, [w | Acc]).
parse_instructions(Line) -> parse_instructions(Line, []).



move(e, {X, Y}) -> {X + 1, Y};
move(w, {X, Y}) -> {X - 1, Y};
move(se, {X, Y}) -> {X, Y + 1};
move(nw, {X, Y}) -> {X, Y - 1};
move(sw, {X, Y}) -> {X - 1, Y + 1};
move(ne, {X, Y}) -> {X + 1, Y - 1}.


follow([], Pos) -> Pos;

follow([NextMove | Instructions], Pos) ->
    follow(Instructions, move(NextMove, Pos)).

follow(Instructions) ->
    follow(Instructions, {0, 0}).


flip(Pos, Map) ->
    case sets:is_element(Pos, Map) of
        true -> sets:del_element(Pos, Map);
        false -> sets:add_element(Pos, Map)
    end.


setup([], Map) -> Map;

setup([Head | Tail], Map) ->
    Pos = follow(parse_instructions(Head)),
    setup(Tail, flip(Pos, Map)).

setup(Lines) ->
    setup(Lines, sets:new()).


neighbors(Pos) ->
    [move(Direction, Pos) || Direction <- [e, w, se, nw, sw, ne]].


total_active_neighbors(Pos, Map) ->
    lists:foldl(
        fun (Neighbor, Acc) ->
            case sets:is_element(Neighbor, Map) of
                true -> Acc + 1;
                false -> Acc
            end
        end, 0, neighbors(Pos)).


step_active(Map, Output) ->
    sets:fold(
        fun (Pos, Acc) ->
            case total_active_neighbors(Pos, Map) of
                1 -> sets:add_element(Pos, Acc);
                2 -> sets:add_element(Pos, Acc);
                _ -> Acc
            end
        end, Output, Map).


inactive_neighbors(Map) ->
    sets:fold(
        fun (Pos, Acc) ->
            Neighbors = lists:filter(
                fun (Neighbor) -> not sets:is_element(Neighbor, Map) end,
                neighbors(Pos)),
            sets:union(Acc, sets:from_list(Neighbors))
        end, sets:new(), Map).


step_inactive(Map, Output) ->
    sets:fold(
        fun (Pos, Acc) ->
            case total_active_neighbors(Pos, Map) of
                2 -> sets:add_element(Pos, Acc);
                _ -> Acc
            end
        end, Output, inactive_neighbors(Map)).


step(Map) ->
    step_inactive(Map, step_active(Map, sets:new())).


run(0, Grid) -> Grid;
run(Steps, Grid) ->
    run(Steps - 1, step(Grid)).


solve_part1(Input) ->
    Map = setup(Input),
    sets:size(Map).


solve_part2(Input) ->
    Map = run(100, setup(Input)),
    sets:size(Map).


test() ->
    Input = read_input("test.txt"),
    ?assertEqual(10, solve_part1(Input)),
    ?assertEqual(15, sets:size(run(1, setup(Input)))),
    ?assertEqual(2208, solve_part2(Input)).


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
