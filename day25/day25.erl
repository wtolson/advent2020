-module(day25).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").

-define(SUBJECT, 7).
-define(DIVISOR, 20201227).


split(Subject, nl) -> split(Subject, <<"\n">>);
split(Subject, Pattern) ->
    binary:split(Subject, Pattern, [global, trim]).


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    [CardPubKey, DoorPubKey] = split(Data, nl),
    {binary_to_integer(CardPubKey), binary_to_integer(DoorPubKey)}.


find_encryption_key(CardPubKey, _DoorPubKey, CardPubKey, Key) -> Key;
find_encryption_key(CardPubKey, DoorPubKey, Value, Key) ->
    NextValue = (Value * ?SUBJECT) rem ?DIVISOR,
    NextKey = (Key * DoorPubKey) rem ?DIVISOR,
    find_encryption_key(CardPubKey, DoorPubKey, NextValue, NextKey).

find_encryption_key(CardPubKey, DoorPubKey) ->
    find_encryption_key(CardPubKey, DoorPubKey, 1, 1).


solve_part1({CardPubKey, DoorPubKey}) ->
    find_encryption_key(CardPubKey, DoorPubKey).


test() ->
    ?assertEqual(14897079, find_encryption_key(5764801, 17807724)).


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    merrychristmas.
