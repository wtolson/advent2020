-module(dayXX).
-export([test/0, part1/0, part2/0]).
-include_lib("stdlib/include/assert.hrl").


enumerate(List) ->
    enumerate(List, 0, 1).
enumerate(List, Start) ->
    enumerate(List, Start, 1).
enumerate(List, Start, Incr) ->
    lists:zip(lists:seq(Start, length(List) + Start - 1, Incr), List).


enumerated_foldl(Fun, Acc, List) ->
    enumerated_foldl(Fun, Acc, List, 0, 1).
enumerated_foldl(Fun, Acc, List, Start) ->
    enumerated_foldl(Fun, Acc, List, Start, 1).
enumerated_foldl(_Fun, Acc, [], _Start, _Incr) -> Acc;
enumerated_foldl(Fun, Acc, [Elem | Tail], Start, Incr) ->
    enumerated_foldl(Fun, Fun(Start, Elem, Acc), Tail, Start + Incr, Incr).


split(Subject, nl) -> split(Subject, <<"\n">>);
split(Subject, nlnl) -> split(Subject, <<"\n\n">>);
split(Subject, space)  -> split(Subject, <<" ">>);
split(Subject, comma)  -> split(Subject, <<", ">>);
split(Subject, Pattern) ->
    binary:split(Subject, Pattern, [global, trim]).


read_input(Filename) ->
    {ok, Data} = file:read_file(Filename),
    [binary_to_integer(Line) || Line <- split(Data, nl)].


solve_part1(Input) ->
    todo.


solve_part2(Input) ->
    todo.


test() ->
    Input = read_input("test.txt"),
    ?assertEqual(todo, solve_part1(Input)),
    ?assertEqual(todo, solve_part2(Input)).


part1() ->
    Input = read_input("input.txt"),
    solve_part1(Input).


part2() ->
    Input = read_input("input.txt"),
    solve_part2(Input).
